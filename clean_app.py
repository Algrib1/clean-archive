import os
import sys
import shutil
import logging
import tempfile


def checker(ls: list):
    keyword = "__init__.py"
    print(ls)
    for i in ls:
        print(i)
        if i == keyword:
            return False
    return True


def main():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    archive_name = sys.argv[1]
    name_w_ext = os.path.basename(archive_name)

    if os.path.exists(archive_name) and archive_name.endswith('.zip'):
        with tempfile.TemporaryDirectory() as tempdir:
            log_path = f"{tempdir}/cleaned.txt"
            fh = logging.FileHandler(log_path)
            fh.setLevel(logging.DEBUG)
            logger.addHandler(fh)
            shutil.unpack_archive(archive_name, tempdir)
            for path, dirs, files in os.walk(tempdir):
                if path == tempdir:
                    print("")
                else:
                    if checker(files):
                        logger.info(path.split(tempdir, 1)[-1][1:])
                        shutil.rmtree(path)
            name_split = name_w_ext.split(".")
            os.system(f'sort {log_path} -o {log_path}')
            shutil.make_archive(f"{name_split[0]}_new", "zip", tempdir)
    else:
        print("Wrong filepath, could not find .zip archive")


if __name__ == "__main__":
    main()
